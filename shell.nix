with (import <nixpkgs> {});
let
  env = bundlerEnv {
    name = "PyGotham-2019-Bundler-Env";
    inherit ruby;
    gemfile  = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset   = ./gemset.nix;
  };
in stdenv.mkDerivation {
  name = "PyGotham-2019";
  buildInputs = [ env ];
}
